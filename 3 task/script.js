function fullMoving(first,second){
        var input = document.getElementById(first);
        var output = document.getElementById(second);
        for (var i = 0; i < input.children.length; i++){
            output.appendChild(input.children[i]);
            i--;
        }
}

  function separateMoving(first,second){
    var empty = true;
    const input = document.getElementById(first);
    const inputValues = input.children;
    const output = document.getElementById(second);
    for (var i = 0; i < inputValues.length; i++){
        let current = inputValues[i];
        if ((current.selected) === true){
                    var k = output.appendChild(current);
                    k.selected = false;
                    i--;
                    empty = false;
        }
    }
    if (empty === true){
        alert("At least one value should be dedicated");
    }
}

  document.getElementById('b1').onclick = function(){
      fullMoving("left","right");
  }
  document.getElementById('b2').onclick = function(){
      separateMoving("left","right");
  }
  document.getElementById('b3').onclick = function(){
      separateMoving("right","left");
  }
  document.getElementById('b4').onclick = function(){
    fullMoving("right","left");
  }