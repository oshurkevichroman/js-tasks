document.getElementById('b').onclick = checkAndDo;

var operations  = ["+", "-", "*", "/", "="];
var input = document.getElementById("input");
var output = document.getElementById("output");
function checkAndDo(){
var regEx = /(\d+(\.\d+)*[\+\-\*\/]{1}\d+(\d+\.\d+)*)(\ *[\+\-\*\/]{1}\d(\.\d+)*)*\=/g;
if (input.value.match(regEx)){
    evaluateRegEx();
}
else {
    alert("Wrong expression");
}
}

function evaluateRegEx(){
    var inputValue = input.value;
    var arrayOfOperations = [];
    var res = 0;

    for (var i = 0; i < inputValue.length; i++){
        for (var j = 0; j < operations.length; j++){
            if (inputValue[i] === operations[j]){
                arrayOfOperations.push(inputValue[i]);
            }
        }
    }
    console.log(arrayOfOperations);
    inputValue = inputValue.replace(/ */g, "");
    var numbersArray = inputValue.split(/[\+\-\*\/\=]/);
    console.log(numbersArray);

    for (var i  = 0; i < arrayOfOperations.length - 1; i++){
        if (i == 0){
        var firstVal = Number(numbersArray[i]);
        var secondVal = Number(numbersArray[i+1]);
        var operation;
       if (arrayOfOperations[i] == "+"){
           operation = plus(firstVal, secondVal);
       }
       else if (arrayOfOperations[i] == "-"){
           operation = minus(firstVal,secondVal);
       }
       else if (arrayOfOperations[i] == "*"){
           operation = multiply(firstVal,secondVal);
       }
       else if (arrayOfOperations[i] == "/"){
           operation = divide(firstVal,secondVal);
       }
       res = operation;
    }
else {
    var firstVal = res;
    var secondVal = parseInt(numbersArray[i+1]);
    var operation;
   if (arrayOfOperations[i] == "+"){
       operation = plus(firstVal, secondVal);
   }
   else if (arrayOfOperations[i] == "-"){
       operation = minus(firstVal,secondVal);
   }
   else if (arrayOfOperations[i] == "*"){
       operation = multiply(firstVal,secondVal);
   }
   else if (arrayOfOperations[i] == "/"){
       operation = divide(firstVal,secondVal);
   }
   res = operation.toFixed(2);
}
}
   output.value = res;

}

function plus(a,b){
    return a + b;
}

function minus(a,b){
    return a - b;
}

function multiply(a,b){
    return a * b;
}

function divide(a,b){
    return a / b;
}
